<?php

use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($x=1;$x<=20;$x++){
            DB::table('tasks')->insert([
                'description' => str_random(5),
                'project_id' => rand(1,5)
            ]);
        }
    }
}
