<?php

//app()->singleton('App\Services\Twitter', function(){
//    return new App\Services\Twitter('dasdfdjkfnskj');
//});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//app(Filesystem::class)

//Route::get('/', function(){
//    dd(app('Illuminate\Filesystem\Filesystem'));
//});


Route::get('/contact', 'PagesController@contact');

Route::get('/about', 'PagesController@about');


Route::get('/', 'PagesController@home');


Route::resource('projects', 'ProjectsController');

Route::patch('/tasks/{task}', 'ProjectTaskController@update');

Route::patch('/projects/{project}/tasks', 'ProjectTaskController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
