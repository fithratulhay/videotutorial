@extends('layout')

@section('content')
    
    <h1>{{ $me }} LEARNING</h1> 
    
    {{"My Task :"}}
    
    <ul>
    
    @foreach($tasks as $task)
        <li>{{ $task }}</li>
    @endforeach
    
    </ul>

@endsection

