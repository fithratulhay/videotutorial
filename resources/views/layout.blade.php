<!DOCTYPE html>
<html>
<head>
    <title>@yield('title', 'Belajar Laravel')</title>
</head>
<body>
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>
        
                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    @yield('content')

    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about">About Us</a></li>  
        <li><a href="/contact">Contact</a> us to learn more </li>
        <li><a href="/projects">Our Projects</a></li>
    </ul>

</body>
</html>