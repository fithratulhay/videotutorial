<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <h1>PROJECTS</h1>

    <ul>
        @foreach ($projects as $p)
            <li>
                <a href="/projects/{{$p->id}}">
                    {{ $p->title }}
                </a>
            </li>
        @endforeach
    </ul>
    
    <br>
    <br>

    <li><a href="/projects/create">Create New Project</a></li>
</body>
</html>