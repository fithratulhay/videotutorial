@extends('projects.projectlayout')

@section('content')
    <h1 class="title">{{$project->title}}</h1>

    <div class="content">
        {{$project->description}}
    </div>

    <p>
        <a href="/projects/{{$project->id}}/edit">Edit</a>
    </p>
    <br>

    @if($project->tasks->count())
        <div>
            @foreach($project->tasks as $task)
                <div>
                    <form method="POST" action="/tasks/{{$task->id}}">
                        @method('PATCH')
                        @csrf

                        <label class="checkbox" for="completed">
                            <input type="checkbox" name="completed" onChange="this.form.submit()" {{ $task->completed ? 'checked' : ''}}>
                            {{$task->description}}
                        </label> 
                    </form>
                </div>
            @endforeach
        </div>
    @endif
    <br>
    <form method="POST" action="/projects/{{$project->id}}/tasks" style="margin-bottom : 0.5cm">
        {{ method_field('PATCH')}}
        {{ csrf_field() }}
        <div class="field">
            <label class="label" for="title">Add Task</label>

            <div class="control">
                <input type="text" class="input" name="description" placeholder="Task" value="task description" required>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Add</button>    
            </div>
        </div>

        @if ($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
        @endif
        
    </form>

@endsection
