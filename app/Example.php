<?php

namespace App;

class Example
{
    protected $something;

    public function __construct(Something $something)
    {
        $this->something = $something;    
    }
}