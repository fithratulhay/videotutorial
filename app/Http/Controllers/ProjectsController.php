<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Illuminate\Filesystem\Filesystem;
use App\Services\Twitter;
use App\User;
use App\Mail\ProjectCreated;
use Illuminate\Support\Facades\Mail;

class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $projects = Project::where('user_id', auth()->id())->get();

        return view('projects.index', ['projects' => $projects]);
    }

    public function create(){
        
        return view('projects.create');
    
    }

    public function store(User $user){
        $validated = request()->validate([
            'title'=>['required', 'min:3'],
            'description' => 'required'
        ]);

        $validated['user_id'] = auth()->id();
        
        $project = Project::create($validated);
        
        event(new ProjectCreated($project));

        return redirect('/projects');
    }

    public function edit(Project $project){

        return view('projects.edit', compact('project'));
    
    }
    
    public function update(Project $project){
        request()->validate([
            'title'=>'required',
            'description' => 'required'
        ]);

        $project->update(request(['title','description']));

        return redirect('/projects'); 
    }

    public function destroy(Project $project){
        $project->delete();
        
        return redirect('/projects');
    }

    public function show(Project $project){
        if($project->user_id !== auth()->id()){
            abort(403);
        }
        //dd($twitter);
        return view('projects.show', compact('project'));
    
    }
}
