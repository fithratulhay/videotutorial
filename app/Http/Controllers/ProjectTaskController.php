<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tasks;
use App\Project;

class ProjectTaskController extends Controller
{
    public function update(tasks $task){
        //return $task;
        $task->update([
            'completed' => request()->has('completed')
        ]);
        

        return back();
    }

    public function store(Project $project){
        //dd($project->tasks->map->description);
        $attribute = request()->validate([
            'description' => 'required' 
        ]);
        
        $project->addTask($attribute);

        return back();
    }
}
