<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProjectCreated;
use App\Events\ProjectCreated as AppProjectCreated;

class Project extends Model
{
    protected $fillable=[
        'title','description','user_id'
    ];

    protected static function boot(){
        parent::boot();

        static::created(function($project){
             
        });
    }

    protected $dispatchesEvents = [
        'created' => AppProjectCreated::class
    ];

    public function tasks(){
        return $this->hasMany(tasks::class);
    }
    
    public function addTask($task){
        $this->tasks()->create($task);
    }

    public function User(){
        return $this->belongsTo(User::class);
    }
}
